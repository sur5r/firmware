/*******************************************************************************
 * Copyright (C) 2020 Maxim Integrated Products, Inc., All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL MAXIM INTEGRATED BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of Maxim Integrated
 * Products, Inc. shall not be used except as stated in the Maxim Integrated
 * Products, Inc. Branding Policy.
 *
 * The mere transfer of this software does not imply any licenses
 * of trade secrets, proprietary technology, copyrights, patents,
 * trademarks, maskwork rights, or any other form of intellectual
 * property whatsoever. Maxim Integrated Products, Inc. retains all
 * ownership rights.
 *
 * $Date: 2020-08-28 21:41:37 +0000 (Fri, 28 Aug 2020) $
 * $Revision: 54997 $
 *
 ******************************************************************************/

#ifndef BAS_USER_H
#define BAS_USER_H

/**************************************************************************************************
  Macros
**************************************************************************************************/
   
#define INTERVAL_TIME_OST_STARTUP   SEC(5)       // (s) before advertising
#define INTERVAL_TIME_OST_BASREAD   MSEC(100)    // (ms) delay to read battery level
#define OST_TIMER           MXC_TMR4  // Can be MXC_TMR0 through MXC_TMR5   
#define OST_TIMER_IDX       MXC_TMR_GET_IDX(OST_TIMER)

// Specify that the OST_TIMER be used to simulate the delay between
// the battery level read being requested, and actually executed.
// If this option is not defined, then the interrupt handler will be called when
// wsfOsDispatcher has no pending events or messages.
//#define USE_BAS_ASYNC_READ_TIMER_DELAY

// Specify the return code for the AsyncReadCallback when in the NONE state.
//#define BAS_ASYNC_READ_PENDING_RSP ATT_RSP_PENDING
#define BAS_ASYNC_READ_NONE_RSP ATT_SUCCESS

// Specify the return code for the AsyncReadCallback when in the PENDING state.
//#define BAS_ASYNC_READ_PENDING_RSP ATT_ERR_IN_PROGRESS
//#define BAS_ASYNC_READ_PENDING_RSP ATT_RSP_PENDING
#define BAS_ASYNC_READ_PENDING_RSP ATT_SUCCESS

/**************************************************************************************************
  Type definitions
**************************************************************************************************/

/* battery level read state. */
typedef enum {
    BAS_READ_STATE_NONE = 0,
    BAS_READ_STATE_PENDING,
    BAS_READ_STATE_READY
} BAS_READ_STATE;

/**************************************************************************************************
  Global Variable Definitions
**************************************************************************************************/
#ifndef BAS_USER_C
extern BAS_READ_STATE eBasAsyncReadState;
#endif /* BAS_USER_C */

void BasReadAsyncHandler();
uint8_t BasReadAsyncCback(
        dmConnId_t connId, uint16_t handle, uint8_t operation,
        uint16_t offset, attsAttr_t *pAttr);

#endif /* BAS_USER_H */
