.. _card10_cfg:

card10.cfg
==========

Certain high-level settings can be configured using a filed named ``card10.cfg``.  It is accessed from the :ref:`usb_file_transfer` of the bootloader.  Once you are in this mode and have mounted the badge's flash device, you can either create or update a file named ``card10.cfg``.

The file is in the well-known INI-style format, with one setting per line. For instance, if there were an option called ``answer_to_life``, you could set it by writing the following line in the ``card10.cfg`` file:

.. code-block:: text

   answer_to_life = 42

Don't forget to unmount the filesystem before rebooting your badge after changing any setting.

Syntax and Types
----------------

Lines that start with a ``#`` character are ignored.

Any other line will have the overall syntax of ``option_name = option_value``, with spaces around the ``=`` character optional.

Option names are internal to card10 and described below. Each option has a defined type.

========= ===========
Type name Description
========= ===========
Boolean   A true/false value. ``1`` or ``true`` is true, ``0`` or ``false`` is false. Example: ``foo = true``.
String    An unquoted string value of maximum 20 bytes. Values longer than 20 bytes are trimmed. Example: ``foo = bar``.
Integer   A signed 32-bit integer in base 10. Example: ``foo = 42`` or ``bar = -1337``.
Float     A single-precision (32-bit) floating-point number in base 10. Example: ``foo = 13.37``.
========= ===========

Supported options
-----------------

================== ========== ===========
Option name        Type       Description
================== ========== ===========
``execute_elf``    Boolean    Allow running of binary :ref:`l0dables`. These files can be nefarious, so this option is off by default.
------------------ ---------- -----------
``timezone``       String     Timezone for card10; must be of format ``[+-]HHMM``.  Examples: ``+0800``, ``-0200``
------------------ ---------- -----------
``default_app``    String     Full path to the exectutable file of the default application. If this option is not set,``apps/analog_clock/__init__.py`` is used.
------------------ ---------- -----------
``ble_enable``     Boolean    Activate the BLE interface. Turn off for more privacy or to conserve energy.
------------------ ---------- -----------
``ble_mac``        Boolean    MAC address used for BLE. Format: ``ca:4d:10:xx:xx:xx``.
------------------ ---------- -----------
``ble_hid_enable`` Boolean    Enable the Human Interface Device (HID) characteristics on BLE.
------------------ ---------- -----------
``ble_log_enable`` Boolean    Activate HCI level logging of BLE data. Creates a new btsnoop compatible log file named ``ble.log`` in the ``logs`` folder after each boot if BLE is activated. Keeps the last 10 files.
------------------ ---------- -----------
``right_scroll``   Boolean    Use both right buttons to scroll up and down. Lower left button is SELECT.
------------------ ---------- -----------
``long_press_ms``  Integer    Defines the timespan for a long key press in milliseconds.
------------------ ---------- -----------
``retrigger_ms``   Integer    Defines the timespan for repeating key presses when a key is hold in milliseconds.
------------------ ---------- -----------
``bsec_enable``    Boolean    Activate the Bosch :ref:`bsec_api` binary blob to compute an Indoor Air Quality indication.
------------------ ---------- -----------
``bsec_debug``     Boolean    Turn on debug output of the BSEC system. Prints each meaurement on the console.
------------------ ---------- -----------
``bsec_offset``    Integer    Temperature offset in .1 K. Example: Set to `-14` if temperature reads 1.4 "C" to high. Default: -2.2 K (appropriate for a card10 without a case, connected to USB and with BLE active in vertical orientation).
------------------ ---------- -----------
``battery_check``  Boolean    Whether the low battery check should be enabled (default ``true``).  **Warning**: Do not use this unless you know what you're doing.  This option is only meant to be used on devices with a broken PMIC ADMUX connection.
================== ========== ===========
