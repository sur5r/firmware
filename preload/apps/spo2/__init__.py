import max86150
import display
import time
import buttons
import spo2_algo
import color


class SPO2:
    def __init__(self):
        self.sensor = None
        self.RATE = 100
        self.HISTORY_MAX = self.RATE * 5
        self.ir_history = []
        self.red_history = []
        self.update_screen = 0
        self.disp = display.open()
        self.DRAW_AFTER_SAMPLES = 100
        self.histogram_offset = 0
        self.WIDTH = 160
        self.SCALE_FACTOR = 30
        self.OFFSET_Y = 49
        self.COLOR_BACKGROUND = [0, 0, 0]
        self.avg = [0] * 10
        self.avg_pos = 0
        self.last_sample = 0.0
        self.filtered_value = 0.0
        self.average = 0
        self.prev_w_ir = 0
        self.prev_w_red = 0
        self.t0 = time.time_ms()

    def open(self):
        def callback(datasets):
            self.update_screen += len(datasets)

            self.update_history(datasets)

            # don't update on every callback
            if self.update_screen >= self.DRAW_AFTER_SAMPLES:
                self.disp.clear(self.COLOR_BACKGROUND)
                self.draw_history(self.ir_history, color.RED)
                self.draw_history(self.red_history, color.GREEN)
                spo2, spo2_valid, hr, hr_valid = spo2_algo.maxim_rd117(
                    self.ir_history, self.red_history
                )

                print(time.time_ms() - self.t0, spo2, spo2_valid, hr, hr_valid)
                self.t0 = time.time_ms()
                if hr_valid:
                    self.disp.print("HR: {0:3} bpm".format(hr), posy=20)
                else:
                    self.disp.print("HR: --- bpm".format(hr), posy=20)

                if spo2_valid:
                    self.disp.print("SpO2: {0:3}%".format(spo2), posy=0)
                else:
                    self.disp.print("SpO2: ---%".format(spo2), posy=0)

                self.disp.update()
                """
                if hr_valid and hr > x:
                    with open("ir.txt", "w") as ir:
                        for sample in self.ir_history:
                            ir.write("%d\n" % sample)

                    with open("red.txt", "w") as ir:
                        for sample in self.red_history:
                            ir.write("%d\n" % sample)

                    print("Wrote data")
                    while True: pass
                """
                self.update_screen = 0

        # 4x over sampling is active ATM
        self.sensor = max86150.MAX86150(callback=callback, sample_rate=self.RATE * 4)

    def update_history(self, datasets):
        for val in datasets:
            # print("%d,%d" % (val.red, val.infrared))

            self.ir_history.append(val.infrared)
            self.red_history.append(val.red)

            """
            w_ir = val.infrared + self.prev_w_ir * 0.95
            self.ir_history.append(w_ir - self.prev_w_ir)
            self.prev_w = w_ir

            w_red = val.red + self.prev_w_red * 0.95
            self.red_history.append(w - self.prev_w_red)
            self.prev_w = w_red
            """

            """
            self.avg[self.avg_pos] = d

            if self.avg_pos < 9:
                self.avg_pos += 1
            else:
                self.avg_pos = 0

            avg_data = sum(self.avg) / 10
            # DC offset removal
            self.filtered_value = 0.9 * (
                self.filtered_value + avg_data - self.last_sample
            )
            self.last_sample = avg_data
            self.ir_history.append(self.filtered_value)
            """

        # trim old elements
        self.ir_history = self.ir_history[-self.HISTORY_MAX :]
        self.red_history = self.red_history[-self.HISTORY_MAX :]

    def draw_history(self, history, col):
        # offset in pause_histogram mode
        window_end = len(history) - self.histogram_offset
        s_start = max(0, window_end - (self.RATE * 2))
        s_end = max(0, window_end)
        s_draw = max(0, s_end - self.WIDTH)

        average = sum(history[s_start:s_end]) / (s_end - s_start)
        # get max value and calc scale
        value_max = max(abs(x - average) for x in history[s_start:s_end])
        scale = self.SCALE_FACTOR / (value_max if value_max > 0 else 1)

        # draw histogram
        draw_points = (
            int((x - average) * scale + self.OFFSET_Y) for x in history[s_draw:s_end]
        )

        prev = next(draw_points)
        for x, value in enumerate(draw_points):
            self.disp.line(x, prev, x + 1, value, col=col)
            prev = value

    def close(self):
        if self.self is not None:
            self.sensor.close()
            self.sensor = None


if __name__ == "__main__":
    disp = display.open()
    disp.clear()
    disp.print("ATTENTION:", posy=0)
    disp.print("DEMO ONLY!", posy=20, fg=color.RED)
    disp.print("RESULTS ARE", posy=40)
    disp.print("WRONG!", posy=60, fg=color.RED)
    disp.update()

    time.sleep(5)

    sensor = SPO2()
    try:
        sensor.open()
    except KeyboardInterrupt as e:
        sensor.close()
        raise e

    while True:
        time.sleep(0.1)
        if buttons.read(buttons.BOTTOM_RIGHT):
            pass
        while buttons.read(buttons.BOTTOM_RIGHT):
            pass
