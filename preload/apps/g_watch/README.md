#G-Watch
A gesture controlled power-saving digital clock. It can replace main.py as standard watch. 

###How to use: 
Hold your arm roughly straight and turn your wrist quickly. The digital clock will appear on the screen and fade out after a few seconds.

###Features:
* Gesture controlled
* Screen brightness adapts to environment brightness

Uses code from watch++