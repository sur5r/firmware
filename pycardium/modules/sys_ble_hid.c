#include "epicardium.h"

#include "py/builtin.h"
#include "py/obj.h"
#include "py/runtime.h"

static mp_obj_t mp_sys_ble_hid_send_report(mp_obj_t report_id, mp_obj_t data)
{
	mp_buffer_info_t bufinfo;
	mp_get_buffer_raise(data, &bufinfo, MP_BUFFER_READ);

	int ret = epic_ble_hid_send_report(
		mp_obj_get_int(report_id), bufinfo.buf, bufinfo.len
	);

	if (ret == -EAGAIN) {
		mp_raise_msg(&mp_type_MemoryError, NULL);
	}

	if (ret < 0) {
		mp_raise_OSError(-ret);
	}

	return ret == 0 ? mp_const_true : mp_const_false;
}
static MP_DEFINE_CONST_FUN_OBJ_2(
	sys_ble_hid_send_report_obj, mp_sys_ble_hid_send_report
);

static const mp_rom_map_elem_t sys_ble_hid_module_globals_table[] = {
	{ MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_sys_ble_hid) },
	{ MP_ROM_QSTR(MP_QSTR_send_report),
	  MP_ROM_PTR(&sys_ble_hid_send_report_obj) },
};
static MP_DEFINE_CONST_DICT(
	sys_ble_hid_module_globals, sys_ble_hid_module_globals_table
);

const mp_obj_module_t sys_ble_hid_module = {
	.base    = { &mp_type_module },
	.globals = (mp_obj_dict_t *)&sys_ble_hid_module_globals,
};

/* clang-format off */
MP_REGISTER_MODULE(MP_QSTR_sys_ble_hid, sys_ble_hid_module, MODULE_HID_ENABLED);
