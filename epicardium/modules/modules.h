#ifndef MODULES_H
#define MODULES_H

#include "FreeRTOS.h"
#include "gpio.h"
#include "os/mutex.h"
#include "epicardium.h"

#include <stdint.h>
#include <stdbool.h>

/* ---------- Hardware Init & Reset ---------------------------------------- */
int hardware_early_init(void);
int hardware_init(void);
int hardware_reset(void);

/* ---------- LED Animation / Personal States ------------------------------ */
#define PERSONAL_STATE_LED 14
void vLedTask(void *pvParameters);
int personal_state_enabled();

/* ---------- BLE ---------------------------------------------------------- */
void vBleTask(void *pvParameters);
bool ble_is_enabled(void);
void ble_uart_write(uint8_t *pValue, uint8_t len);

/* ---------- Hardware (Peripheral) Locks ---------------------------------- */
void hwlock_init(void);

enum hwlock_periph {
	HWLOCK_I2C = 0,
	HWLOCK_ADC,
	HWLOCK_LED,
	HWLOCK_SPI_ECG,
	_HWLOCK_MAX,
};

void hwlock_acquire(enum hwlock_periph p);
int hwlock_acquire_nonblock(enum hwlock_periph p);
void hwlock_release(enum hwlock_periph p);

#endif /* MODULES_H */
