#pragma once

#include "wsf_types.h"
#include "wsf_os.h"

/* The input report fits in one byte */
#define HIDAPP_KEYBOARD_INPUT_REPORT_LEN  8
#define HIDAPP_MOUSE_INPUT_REPORT_LEN     4
#define HIDAPP_CONSUMER_INPUT_REPORT_LEN  2
#define HIDAPP_OUTPUT_REPORT_LEN          1
#define HIDAPP_FEATURE_REPORT_LEN         1


/* HID Report IDs */
#define HIDAPP_KEYBOARD_REPORT_ID         1
#define HIDAPP_MOUSE_REPORT_ID            2
#define HIDAPP_CONSUMER_REPORT_ID         3

void hid_init(void);
void HidProcMsg(wsfMsgHdr_t *pMsg);
