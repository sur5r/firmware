#pragma once

#include "epicardium.h"

#include <stdint.h>
#include "wsf_types.h"

#include "att_api.h"
#include "dm_api.h"

#define CARD10_UUID_SUFFIX                                                     \
	0x42, 0x23, 0x42, 0x23, 0x42, 0x23, 0x42, 0x23, 0x42, 0x23, 0x42, 0x23
#define CARD10_UUID_PREFIX 0x02, 0x23, 0x42

/**************************************************************************************************
  Data Types
**************************************************************************************************/

/*! Application message type */
typedef union
{
  wsfMsgHdr_t     hdr;
  dmEvt_t         dm;
  attsCccEvt_t    ccc;
  attEvt_t        att;
} bleMsg_t;

/**************************************************************************************************
  Function Declarations
**************************************************************************************************/
/*************************************************************************************************/
/*!
 *  \brief  Start the application.
 *
 *  \return None.
 */
/*************************************************************************************************/
void BleStart(void);

/* ATT client module interface. Used by main BLE module */
void bleValueUpdate(attEvt_t *pMsg);
void bleDiscCback(dmConnId_t connId, uint8_t status);

void ble_epic_att_api_init(void);
void ble_epic_att_api_event(attEvt_t *att_event);
void ble_epic_att_api_free_att_write_data(struct epic_att_write *w);

void ble_epic_ble_api_trigger_event(enum epic_ble_event_type type, void *data);
void ble_epic_ble_api_init(void);
void ble_epic_dm_api_event(dmEvt_t *dm_event);
void ble_epic_disc_cfg_complete(void);

void ble_adv_init(void);
void ble_adv_setup(void);
void ble_adv_set_interval(uint32_t interval_ms);
void ble_adv_stop(void);
void ble_adv_start(uint8_t mode);
void ble_adv_discoverable(bool discoverable);
void ble_adv_proc_msg(bleMsg_t *pMsg);
