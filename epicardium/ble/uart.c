#include "uart.h"
#include "cccd.h"

#include "modules/modules.h"
#include "drivers/drivers.h"

#include "wsf_types.h"
#include "util/bstream.h"
#include "att_api.h"
#include "dm_api.h"
#include "app_api.h"

#include "FreeRTOS.h"
#include "timers.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/**************************************************************************************************
 Handles
**************************************************************************************************/

/* clang-format off */
static const uint8_t UARTSvc[] = {0x9E,0xCA,0xDC,0x24,0x0E,0xE5,0xA9,0xE0,0x93,0xF3,0xA3,0xB5,0x01,0x00,0x40,0x6E};
static const uint16_t UARTSvc_len = sizeof(UARTSvc);

static const uint8_t uartRxCh[] = {ATT_PROP_WRITE, UINT16_TO_BYTES(UART_RX_HDL), 0x9E,0xCA,0xDC,0x24,0x0E,0xE5,0xA9,0xE0,0x93,0xF3,0xA3,0xB5,0x02,0x00,0x40,0x6E};
static const uint16_t uartRxCh_len = sizeof(uartRxCh);
static const uint8_t attUartRxChUuid[] = {0x9E,0xCA,0xDC,0x24,0x0E,0xE5, 0xA9,0xE0,0x93,0xF3,0xA3,0xB5,0x02,0x00,0x40,0x6E};

static const uint8_t uartTxCh[] = {ATT_PROP_READ | ATT_PROP_NOTIFY, UINT16_TO_BYTES(UART_TX_HDL), 0x9E,0xCA,0xDC,0x24,0x0E,0xE5,0xA9,0xE0,0x93,0xF3,0xA3,0xB5,0x03,0x00,0x40,0x6E};
static const uint16_t uartTxCh_len = sizeof(uartTxCh);
static const uint8_t attUartTxChUuid[] = {0x9E,0xCA,0xDC,0x24,0x0E,0xE5, 0xA9,0xE0,0x93,0xF3,0xA3,0xB5,0x03,0x00,0x40,0x6E};

static uint8_t uartValTxChCcc[] = {UINT16_TO_BYTES(0x0000)};
static const uint16_t uartLenTxChCcc = sizeof(uartValTxChCcc);

static uint8_t ble_uart_tx_buf[20];
static uint16_t ble_uart_buf_tx_fill = 0;
/* clang-format on */

/* Attribute list for uriCfg group */
static const attsAttr_t uartAttrCfgList[] = {
	/* Primary service */
	{
		.pUuid       = attPrimSvcUuid,
		.pValue      = (uint8_t *)UARTSvc,
		.pLen        = (uint16_t *)&UARTSvc_len,
		.maxLen      = sizeof(UARTSvc),
		.settings    = 0,
		.permissions = ATTS_PERMIT_READ,
	},
	/* UART rx characteristic */
	{
		.pUuid       = attChUuid,
		.pValue      = (uint8_t *)uartRxCh,
		.pLen        = (uint16_t *)&uartRxCh_len,
		.maxLen      = sizeof(uartRxCh),
		.settings    = 0,
		.permissions = ATTS_PERMIT_READ,
	},
	/* UART rx value */
	{
		.pUuid       = attUartRxChUuid,
		.pValue      = NULL,
		.pLen        = NULL,
		.maxLen      = 128,
		.settings    = ATTS_SET_WRITE_CBACK | ATTS_SET_VARIABLE_LEN,
		.permissions = ATTS_PERMIT_WRITE | ATTS_PERMIT_WRITE_ENC |
			       ATTS_PERMIT_WRITE_AUTH,
	},
	/* UART tx characteristic */
	{
		.pUuid       = attChUuid,
		.pValue      = (uint8_t *)uartTxCh,
		.pLen        = (uint16_t *)&uartTxCh_len,
		.maxLen      = sizeof(uartTxCh),
		.settings    = 0,
		.permissions = ATTS_PERMIT_READ,
	},
	/* UART tx value */
	{
		.pUuid       = attUartTxChUuid,
		.pValue      = ble_uart_tx_buf,
		.pLen        = &ble_uart_buf_tx_fill,
		.maxLen      = sizeof(ble_uart_tx_buf),
		.settings    = 0,
		.permissions = ATTS_PERMIT_READ | ATTS_PERMIT_READ_ENC |
			       ATTS_PERMIT_READ_AUTH,
	},
	/* UART tx CCC descriptor */
	{
		.pUuid    = attCliChCfgUuid,
		.pValue   = uartValTxChCcc,
		.pLen     = (uint16_t *)&uartLenTxChCcc,
		.maxLen   = sizeof(uartValTxChCcc),
		.settings = ATTS_SET_CCC,
		.permissions =
			(ATTS_PERMIT_READ |
			 ATTS_PERMIT_WRITE) // How about security?
	},

};

static uint8_t UARTWriteCback(
	dmConnId_t connId,
	uint16_t handle,
	uint8_t operation,
	uint16_t offset,
	uint16_t len,
	uint8_t *pValue,
	attsAttr_t *pAttr
) {
	static bool was_r = false;

	int i;
	for (i = 0; i < len; i++) {
		if (pValue[i] == '\n' && !was_r) {
			serial_enqueue_char('\r');
		}
		was_r = pValue[i] == '\r';
		serial_enqueue_char(pValue[i]);
	}

	return ATT_SUCCESS;
}

static bool done;
static bool again;

static void ble_uart_flush(void)
{
	if (ble_uart_buf_tx_fill == 0) {
		return;
	}

	dmConnId_t connId = AppConnIsOpen();
	if (connId != DM_CONN_ID_NONE) {
		if (AttsCccEnabled(connId, UART_TX_CH_CCC_IDX)) {
			done   = false;
			again  = true;
			int t0 = xTaskGetTickCount();

			while (!done && ((xTaskGetTickCount() - t0) < 1000)) {
				if (again) {
					again = false;
					AttsHandleValueNtf(
						connId,
						UART_TX_HDL,
						ble_uart_buf_tx_fill,
						ble_uart_tx_buf
					);
				}
				/* This function is supposed to only be called
				 * from the API scheduler with lowest priority.
				 *
				 * If that is not the case anymore, use a delay
				 * instead of the yield. Ideally refactor to avoid
				 * the delay.
				 */
				//vTaskDelay(5);
				taskYIELD();
			}
		}
	}
	ble_uart_buf_tx_fill = 0;
}

static void ble_uart_write_char(uint8_t c)
{
	ble_uart_tx_buf[ble_uart_buf_tx_fill] = c;
	ble_uart_buf_tx_fill++;

	// TODO: increase buffer if configured MTU allows it
	if (ble_uart_buf_tx_fill == sizeof(ble_uart_tx_buf)) {
		ble_uart_flush();
	}
}

void ble_uart_write(uint8_t *pValue, uint8_t len)
{
	for (int i = 0; i < len; i++) {
		ble_uart_write_char(pValue[i]);
	}

	// TODO schedule timer in a few ms to flush the buffer
	ble_uart_flush();
}

static attsGroup_t uartCfgGroup = {
	.pAttr       = (attsAttr_t *)uartAttrCfgList,
	.writeCback  = UARTWriteCback,
	.startHandle = UART_START_HDL,
	.endHandle   = UART_END_HDL,
};

void bleuart_init(void)
{
	/* Add the UART service */
	AttsAddGroup(&uartCfgGroup);
}

void UartProcMsg(bleMsg_t *pMsg)
{
	if (pMsg->hdr.event == ATTS_HANDLE_VALUE_CNF) {
		if (pMsg->att.handle == UART_TX_HDL) {
			if (pMsg->hdr.status == ATT_SUCCESS) {
				done = true;
			} else if (pMsg->hdr.status == ATT_ERR_OVERFLOW) {
				again = true;
			}
		}
	}
	if (pMsg->hdr.event == DM_CONN_OPEN_IND) {
		ble_uart_buf_tx_fill = 0;
	}
}
