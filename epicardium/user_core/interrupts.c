#include "os/mutex.h"
#include "os/core.h"
#include "epicardium.h"
#include "api/interrupt-sender.h"
#include <assert.h>

struct interrupt_priv {
	/* Whether this interrupt can be triggered */
	bool int_enabled[EPIC_INT_NUM];
	/* Whether this interrupt is waiting to be delivered */
	bool int_pending[EPIC_INT_NUM];
	/* Whether any interrupts are currently waiting to be triggered */
	bool has_pending;
};

static struct interrupt_priv interrupt_data;
static struct mutex interrupt_mutex;
static TaskHandle_t interrupts_task;

void interrupt_trigger(api_int_id_t id)
{
	assert(id < EPIC_INT_NUM);

	mutex_lock(&interrupt_mutex);

	if (interrupt_data.int_enabled[id]) {
		interrupt_data.int_pending[id] = true;
		interrupt_data.has_pending     = true;
		mutex_unlock(&interrupt_mutex);
		xTaskNotifyGive(interrupts_task);
	} else {
		mutex_unlock(&interrupt_mutex);
	}
}

void interrupt_trigger_sync(api_int_id_t id)
{
	assert(id < EPIC_INT_NUM);

	mutex_lock(&interrupt_mutex);
	if (!interrupt_data.int_enabled[id])
		goto out;

	while (!api_interrupt_is_ready())
		;

	api_interrupt_trigger(id);
out:
	mutex_unlock(&interrupt_mutex);
}

/*
 * This function solely exists because of that one use of interrupts that breaks
 * the rules:  The RTC ALARM interrupt is triggered from a hardware ISR where
 * interrupt_trigger_sync() won't work because it needs to lock a mutex.
 *
 * DO NOT USE THIS FUNCTION IN ANY NEW CODE.
 */
void __attribute__((deprecated)) interrupt_trigger_unsafe(api_int_id_t id)
{
	assert(id < EPIC_INT_NUM);

	if (!interrupt_data.int_enabled[id])
		return;

	while (!api_interrupt_is_ready())
		;

	api_interrupt_trigger(id);
}

static void interrupt_set_enabled(api_int_id_t id, bool enabled)
{
	assert(id < EPIC_INT_NUM);

	mutex_lock(&interrupt_mutex);
	interrupt_data.int_enabled[id] = enabled;
	mutex_unlock(&interrupt_mutex);
}

static bool interrupt_get_enabled(api_int_id_t id)
{
	assert(id < EPIC_INT_NUM);

	bool enabled;
	mutex_lock(&interrupt_mutex);
	enabled = interrupt_data.int_enabled[id];
	mutex_unlock(&interrupt_mutex);
	return enabled;
}

void interrupt_init(void)
{
	if (interrupt_mutex.name == NULL)
		mutex_create(&interrupt_mutex);

	api_interrupt_init();

	/* Reset all irqs to disabled */
	for (size_t i = 0; i < EPIC_INT_NUM; i++) {
		interrupt_set_enabled(i, false);
	}

	/* Reset interrupt is always enabled */
	interrupt_set_enabled(EPIC_INT_RESET, true);
}

/* Epic-calls {{{ */
int epic_interrupt_enable(api_int_id_t int_id)
{
	if (int_id >= EPIC_INT_NUM) {
		return -EINVAL;
	}

	interrupt_set_enabled(int_id, true);
	return 0;
}

int epic_interrupt_disable(api_int_id_t int_id)
{
	if (int_id >= EPIC_INT_NUM || int_id == EPIC_INT_RESET) {
		return -EINVAL;
	}

	interrupt_set_enabled(int_id, false);
	return 0;
}

int epic_interrupt_is_enabled(api_int_id_t int_id, bool *enabled)
{
	if (int_id >= EPIC_INT_NUM) {
		return -EINVAL;
	}

	*enabled = interrupt_get_enabled(int_id);
	return 0;
}

/* }}} */

void vInterruptsTask(void *pvParameters)
{
	interrupts_task = xTaskGetCurrentTaskHandle();

	while (true) {
		mutex_lock(&interrupt_mutex);

		if (!interrupt_data.has_pending) {
			/* Wait for a wakeup event from interrupt_trigger() */
			mutex_unlock(&interrupt_mutex);
			ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
			mutex_lock(&interrupt_mutex);
		}

		while (!api_interrupt_is_ready()) {
			mutex_unlock(&interrupt_mutex);
			vTaskDelay(pdMS_TO_TICKS(5));
			mutex_lock(&interrupt_mutex);
		}

		api_int_id_t current_irq = EPIC_INT_NUM;
		for (size_t i = 0; i < EPIC_INT_NUM; i++) {
			if (interrupt_data.int_pending[i]) {
				current_irq                   = i;
				interrupt_data.int_pending[i] = false;
				break;
			}
		}

		if (current_irq == EPIC_INT_NUM) {
			interrupt_data.has_pending = false;
		} else if (interrupt_data.int_enabled[current_irq]) {
			api_interrupt_trigger(current_irq);
		}

		mutex_unlock(&interrupt_mutex);
	}
}
